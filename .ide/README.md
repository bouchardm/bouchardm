# v2beta15.json

v2beta15.json allow you to have autocomplete for the file skaffold.yaml.

See: 
- https://github.com/GoogleCloudPlatform/cloud-code-intellij/issues/2396
- https://github.com/kubernetes/kubernetes/tree/master/api/openapi-spec#x-kubernetes-group-version-kind


