# install

```shell
skaffold dev
echo "$(minikube ip) bouchardm.local" | sudo tee -a /etc/hosts
```

# Namespace

https://brunopaz.dev/blog/setup-a-local-dns-server-for-your-projects-on-linux-with-dnsmasq

sudo vim /etc/NetworkManager/dnsmasq.d/localhost.conf
`address=/local/minikube ip`


add `static domain_name_servers=127.0.1.1` to file `/etc/dhcpcd.conf`

if dns not working reload network `sudo systemctl reload NetworkManager`


sudo systemctl restart NetworkManager


fix chrome warning : chrome://flags/#unsafely-treat-insecure-origin-as-secure add http url
