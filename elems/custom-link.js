window.customElements.define('custom-link', class extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({mode: 'open'});

        this.shadowRoot.innerHTML = `
            <style>
                @import "../tailwind.min.css";
            </style>
    
            <div class="border-b font-semibold">
                <a class="block py-4 w-full" id="link" href="">
                    <svg xmlns="http://www.w3.org/2000/svg" id="main-svg" class="w-5 h-5 fill-current inline hidden" viewBox="0 0 24 24">
                        <path id="svg" d="" fill-rule="evenodd"></path>
                    </svg>
                    <span class="ml-2" id="name"></span>
                </a>
            </div>
        `;
    }

    static get observedAttributes() {
        return ['svg', 'href', 'name'];
    };

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'name':
                this.shadowRoot.querySelector('#name').innerHTML = newValue;
                break;
            case 'href':
                this.shadowRoot.querySelector('#link').href = newValue;
                break;
            case 'svg':
                this.shadowRoot.querySelector('#svg').setAttribute('d', newValue);
                this.shadowRoot.querySelector('#main-svg').classList.toggle('hidden');
                break;
        }
    }
});